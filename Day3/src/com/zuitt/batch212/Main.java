package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // for loop
        for (int i = 0; i < 10; i++) {
            System.out.println("Current Count: " + i);
        }

        // for loop in Arrays
        int[] intArray = {100, 200, 300, 400, 500};

        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }


        // forEach loop
        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        for (String name: nameArray) {
            System.out.println("Name: " + name);
        }

        for (int num: intArray) {
            System.out.println("Num: " + num);
        }

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));

        for (int grade: gradesListA) {
            System.out.println(grade);
        }


        // for loops with Nested Loops
        String[][] classRoom = new String[3][3];

        classRoom[0][0] = "Dahyun";
        classRoom[0][1] = "Mina";
        classRoom[0][2] = "Naeyon";

        classRoom[1][0] = "Luffy";
        classRoom[1][1] = "Nami";
        classRoom[1][2] = "Chopper";

        classRoom[2][0] = "Loid";
        classRoom[2][1] = "Anya";
        classRoom[2][2] = "Yor";

        for (int row = 0; row < classRoom.length; row++) {
            for (int col = 0; col < classRoom[row].length; col++) {
                System.out.println(classRoom[row][col]);
            }
        }


        // while loop
        int x = 0, y = 10;

        while ( x < 10) {
            System.out.println("Loop number: " + x);
            x++;
        }


        // do-while loops
        do {
            System.out.println("Countdown: " + y);
            y--;
        } while (y > 0);


        // [Exception Handling]
        // try-catch-finally statement
        Scanner input = new Scanner(System.in);

        int num1 = 0;

        try {
            System.out.print("Please enter a number from 1 to 10: ");
            num1 = input.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid is not a number");
        }
        catch (Exception e) {
            System.out.println("Invalid input");
        }
        finally {
            if (num1 != 0) {
                System.out.println("The number you entered is: " + num1);
            }
        }

    }

}
