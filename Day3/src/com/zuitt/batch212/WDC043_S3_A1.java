package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("\n[FACTORIAL CALCULATOR]");
        int num = 0;
        boolean hasError = false;

        // Repeatedly ask the user for a valid number input (num > 0 || !string)
        do {
            try {
                System.out.print("Input an integer whose factorial will be computed: ");
                num = in.nextInt();

                if (num <= 0) {
                    System.out.println("[Invalid Input] Please input a number not equal to 0 nor a negative number\n");
                    in.nextLine();
                    hasError = true;
                }

                // An input number bigger than 31 will result to a very large number, Java will not be able to compute it properly
                else if (num > 31) {
                    System.out.println("[Invalid Input] Please input a number less than 31 and greater than 0\n");
                    in.nextLine();
                    hasError = true;
                }

                else {
                    hasError = false;
                }

            }
            catch (Exception e) {
                System.out.println("[Invalid Input] Input a valid number input\n");
                in.nextLine();
                hasError = true;
            }
        } while (hasError);


        // If no errors found in the input process
        if (!hasError) {
            // While Loop
            int result = 1, count = 1;

            while (count <= num) {
                result *= count;
                count++;
            }
            // Result
            System.out.println("\n[WHILE OUTPUT] The factorial of " + num + " is " + result);


            // For Loop
            int forResult = 1;

            for (int i = 1; i <= num; i++) {
                forResult *= i;
            }
            // Result
            System.out.println("[FOR OUTPUT] The factorial of " + num + " is " + forResult);
        }

        in.close();

    }

}
